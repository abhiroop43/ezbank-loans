package dev.abhiroopsantra.loans.service;

import dev.abhiroopsantra.loans.dto.LoansDto;

public interface ILoansService {

    /**
     * Create a new loan for a customer
     *
     * @param mobileNumber - the mobile number of the customer
     */
    void createLoan(String mobileNumber);

    /**
     * Fetch the loan details for the given mobile number
     *
     * @param mobileNumber - the mobile number of the customer
     * @return Loan details for the given mobile number
     */
    LoansDto fetchLoan(String mobileNumber);

    /**
     * Update the loan details for the given mobile number
     *
     * @param loansDto - the request object
     * @return true if the loan details are updated successfully
     */
    boolean updateLoan(LoansDto loansDto);

    /**
     * Delete the loan details for the given mobile number
     *
     * @param mobileNumber - the mobile number of the customer
     * @return true if the loan details are deleted successfully
     */
    boolean deleteLoan(String mobileNumber);
}
