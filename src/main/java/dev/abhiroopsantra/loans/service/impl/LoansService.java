package dev.abhiroopsantra.loans.service.impl;

import dev.abhiroopsantra.loans.constants.LoanConstants;
import dev.abhiroopsantra.loans.dto.LoansDto;
import dev.abhiroopsantra.loans.entity.Loans;
import dev.abhiroopsantra.loans.exception.LoanAlreadyExistsException;
import dev.abhiroopsantra.loans.exception.ResourceNotFoundException;
import dev.abhiroopsantra.loans.mapper.LoansMapper;
import dev.abhiroopsantra.loans.repository.LoansRepository;
import dev.abhiroopsantra.loans.service.ILoansService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service @AllArgsConstructor public class LoansService implements ILoansService {
    private LoansRepository loansRepository;

    /**
     * Create a new loan for a customer
     *
     * @param mobileNumber - the mobile number of the customer
     */
    @Override public void createLoan(String mobileNumber) {
        Optional<Loans> existingLoan = loansRepository.findByLoanNumber(mobileNumber);

        if (existingLoan.isPresent()) {
            throw new LoanAlreadyExistsException("Loan already exists for the mobile number " + mobileNumber);
        }

        loansRepository.save(createNewLoan(mobileNumber));
    }

    private Loans createNewLoan(String mobileNumber) {
        Loans newLoan          = new Loans();
        long  randomLoanNumber = 100000000000L + new Random().nextInt(900000000);
        newLoan.setLoanId(randomLoanNumber);
        newLoan.setLoanNumber(String.valueOf(randomLoanNumber));
        newLoan.setMobileNumber(mobileNumber);
        newLoan.setLoanType(LoanConstants.HOME_LOAN);
        newLoan.setTotalLoan(LoanConstants.NEW_LOAN_LIMIT);
        newLoan.setAmountPaid(0f);
        newLoan.setOutstandingAmount(LoanConstants.NEW_LOAN_LIMIT);
        return newLoan;
    }

    /**
     * Fetch the loan details for the given mobile number
     *
     * @param mobileNumber - the mobile number of the customer
     * @return Loan details for the given mobile number
     */
    @Override public LoansDto fetchLoan(String mobileNumber) {
        Loans loan = loansRepository.findByMobileNumber(mobileNumber).orElseThrow(
                () -> new ResourceNotFoundException("Loan", "mobileNumber", mobileNumber));

        return LoansMapper.mapToLoansDto(loan, new LoansDto());
    }

    /**
     * Update the loan details for the given mobile number
     *
     * @param loansDto - the request object
     * @return true if the loan details are updated successfully
     */
    @Override public boolean updateLoan(LoansDto loansDto) {
        Loans loan = loansRepository.findByLoanNumber(loansDto.getLoanNumber()).orElseThrow(
                () -> new ResourceNotFoundException("Loan", "LoanNumber", loansDto.getMobileNumber()));

        LoansMapper.mapToLoans(loansDto, loan);
        loansRepository.save(loan);

        return true;
    }

    /**
     * Delete the loan details for the given mobile number
     *
     * @param mobileNumber - the mobile number of the customer
     * @return true if the loan details are deleted successfully
     */
    @Override public boolean deleteLoan(String mobileNumber) {
        Loans loan = loansRepository.findByMobileNumber(mobileNumber).orElseThrow(
                () -> new ResourceNotFoundException("Loan", "mobileNumber", mobileNumber));

        loansRepository.deleteById(loan.getLoanId());

        return true;
    }
}
