package dev.abhiroopsantra.loans.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

@Entity @Getter @Setter @ToString @AllArgsConstructor @NoArgsConstructor public class Loans extends BaseEntity {
    @Id
    @Column(name = "loan_id")
    private Long loanId;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "loan_number")
    private String loanNumber;

    @Column(name = "loan_type")
    private String loanType;

    @Column(name = "total_loan")
    private Float totalLoan;

    @Column(name = "amount_paid")
    private Float amountPaid;

    @Column(name = "outstanding_amount")
    private Float outstandingAmount;
}
