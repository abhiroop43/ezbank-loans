-- SELECT 'CREATE DATABASE ezbank_loans'
-- WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'ezbank_loans')
-- \gexec;

create table if not exists loans
(
    loan_id            bigint not null
        primary key,
    created_at         timestamp(6),
    created_by         varchar(255),
    updated_at         timestamp(6),
    updated_by         varchar(255),
    amount_paid        real,
    loan_number        varchar(255),
    loan_type          varchar(255),
    mobile_number      varchar(255),
    outstanding_amount real,
    total_loan         real
);
